import styles from '../../styles/components/Ripple.module.scss'

const Ripple = () => {
  return (
    <div className={styles.Ripple}><div></div><div></div></div>
  )
}

export default Ripple