import styles from '../../styles/components/Navigation.module.scss'
import Link from 'next/link'
import { Component } from 'react'

class Navigation extends Component {
  state = {
    activeMenu: false
  }

  showMenu = event => {
    this.setState({ activeMenu: !this.state.activeMenu })
  }

  render() {
    let menuActiveClass = null
    if(this.state.activeMenu) {
      menuActiveClass = styles.Navigation__profile__active
    }

    return (
      <div className={styles.Navigation}>
        <Link href="/">
          <a className={styles.Navigation__logo}>
            <span className={styles.Navigation__logoBold}>H</span>obby
            <span className={styles.Navigation__logoBold}>S</span>cheduler
          </a>
        </Link>
        <div className={[styles.Navigation__profile, menuActiveClass].join(' ')} onClick={this.showMenu}>
          <div className={styles.Navigation__picture}>
            <img className={styles.Navigation__image} src="/images/common/default-profile.jpg" alt="Profile" />
          </div>
          <div className={styles.Navigation__menuContainer}>
            <div className={styles.Navigation__menu}>
              <Link href="/profile">
                <a className={styles.Navigation__link}>Profile</a>
              </Link>
              <Link href="/settings/hobby/">
                <a className={styles.Navigation__link}>Hobby Editor</a>
              </Link>
              <Link href="/settings/personal/">
                <a className={styles.Navigation__link}>Personal Settings</a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Navigation