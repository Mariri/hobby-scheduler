import Head from 'next/head'

const Header = () => {
  return (
    <Head>
      <title>Hobby Scheduler</title>
      <meta
        name="viewport"
        content="initial-scale=1.0, width=device-width, shrink-to-fit=yes" />
      <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap"
        rel="stylesheet" />
    </Head>
  )
}

export default Header