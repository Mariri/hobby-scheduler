import { Component } from 'react'
import styles from '../../../styles/components/InputItem.module.scss'

class Input extends Component {
  state = {
    passwordStrength: 'weak',
    passwordStrengthClass: styles.InputItem__weak
  }

  passwordMeterEvent = event => {
    let value = event.target.value
    let passwordStrength = this.testPasswordStrength(value)
    let style = null

    if(passwordStrength === 'strong') {
      style = styles.InputItem__strong
    } else if(passwordStrength === 'medium') {
        style = styles.InputItem__medium
    } else if(passwordStrength === 'weak') {
      style = styles.InputItem__weak
    }

    console.log(style)

    this.setState({
      passwordStrength: passwordStrength,
      passwordStrengthClass: style })
  }

  testPasswordStrength = value => {
    let strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[=/\()%ยง!@#$%^&*])(?=.{8,})'),
      mediumRegex = new RegExp('^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})');

    if (strongRegex.test(value)) {
      return 'strong';
    } else if (mediumRegex.test(value)) {
      return 'medium';
    } else {
      return 'weak';
    }
  }

  render() {
    let passwordStrength = null
    let classError = null
    let errorMessage = null
    if(this.props.isRegisterPassword) {
      passwordStrength = (
        <div className={styles.InputItem__strengthMeter}>
          <span className={styles.InputItem__meterStatus}>{this.state.passwordStrength}</span>
          <div className={[styles.InputItem__meterBox, this.state.passwordStrengthClass].join(' ')}>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      )
    }

    if(this.props.error) {
      classError = styles.InputItem__error
      errorMessage = (<span className={styles.InputItem__errorMessage}>$error</span>)
    }

    return (
      <>
        <div className={styles.InputItem}>
          <input
            type={this.props.type}
            name={this.props.name}
            className={[styles.InputItem__input, classError].join(' ')}
            onKeyUp={this.props.isRegisterPassword ? this.passwordMeterEvent : null} />
          <span className={styles.InputItem__label}>{this.props.label}</span>
          {errorMessage}
          {passwordStrength}
        </div>
      </>
    )
  }
}

export default Input