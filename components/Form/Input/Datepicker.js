import React, { useState } from 'react'
import DatePicker from 'react-datepicker'

import styles from '../../../styles/components/InputItem.module.scss'

const Datepicker = (props) => {
  const [startDate, setStartDate] = useState(new Date());

  let classError = null
  let errorMessage = null

  if(props.error) {
    classError = styles.InputItem__error
    errorMessage = (<span className={styles.InputItem__errorMessage}>$error</span>)
  }

  return (
    <div className={styles.InputItem}>
      <DatePicker
        selected={startDate}
        onChange={date => setStartDate(date)}
        name={props.name} />
      <span className={[styles.InputItem__label, styles.InputItem__labelTop].join(' ')}>{props.label}</span>
      {errorMessage}
    </div>
  )
}

export default Datepicker