import styles from '../../../styles/components/InputItem.module.scss'

const Textarea = (props) => {
  return (
    <div className={styles.InputItem}>
      <textarea
        name={props.name}
        className={[styles.InputItem__input, styles.InputItem__inputTextarea].join(' ')}></textarea>
      <span className={styles.InputItem__label}>{props.label}</span>
    </div>
  )
}

export default Textarea