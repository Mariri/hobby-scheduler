import styles from '../../../styles/components/Button.module.scss'

const Button = (props) => {
  return (
    <div className={styles.Button}>
      <svg className={styles.Button__svg}>
        <rect className={styles.Button__rect} x="0" y="0" fill="none" width="100%" height="100%"/>
      </svg>
      <input
        type="submit"
        value={props.value}
        className={styles.Button__input} />
    </div>
  )
}

export default Button