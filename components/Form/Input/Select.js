import styles from '../../../styles/components/InputItem.module.scss'

const Select = (props) => {
  return (
    <div className={styles.InputItem}>
      <label className={[styles.InputItem__label, styles.InputItem__labelTop].join(' ')}>{props.label}</label>
      <select className={[styles.InputItem__input, styles.InputItem__inputSelect].join(' ')} name={props.name}>
        {props.options.map(item => (
          <option
            key={item}
            value={item}>{item}</option>
        ))}
      </select>
    </div>
  )
}

export default Select