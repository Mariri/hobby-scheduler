import Plus from '../../Symbols/Plus'
import styles from '../../../styles/components/InputItem.module.scss'

const Checkbox = (props) => {
  return (
    <div className={styles.InputItem}>
      <label className={[styles.InputItem__label, styles.InputItem__labelTop].join(' ')}>{props.label}</label>
      <div className={styles.InputItem__checkboxContainer}>
        {props.options.map(item => (
          <label
            key={item}
            className={styles.InputItem__checkboxItem}>
            <input
              type="checkbox"
              className={styles.InputItem__checkbox}
              name={props.name}
              value={item} />
            <span className={styles.InputItem__checkboxText}>{item}</span>
          </label>
        ))}
      </div>
      <div className={styles.InputItem__addCategory}>
        <input
          type="text"
          className={styles.InputItem__inputCategory}
          name="category"
          placeholder="Add a category.." />
        <button className={styles.InputItem__plus}>
          <div></div>
          <div></div>
        </button>
      </div>
    </div>
  )
}

export default Checkbox