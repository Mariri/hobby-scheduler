import styles from '../../styles/components/Logo.module.scss'

const Logo = () => {
  return (
    <div className={styles.Logo}>
      <span className={styles.Logo__bold}>H</span>obby
      <span className={styles.Logo__bold}>S</span>cheduler
    </div>
  )
}

export default Logo