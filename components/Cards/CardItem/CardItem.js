import Link from 'next/link'
import { Component } from 'react'

import styles from '../../../styles/components/CardItem.module.scss'

class CardItem extends Component {
  state = {
    activeOption: false
  }

  optionsActive = event => {
    event.preventDefault()
    this.setState({ activeOption: !this.state.activeOption })
  }

  render() {
    let imagePreview = <span className={styles.CardItem__noImage}>No image preview</span>
    if(this.props.image) {
      imagePreview = <img src={this.props.image} className={styles.CardItem__image} />
    }

    let optionActiveClass = null
    if(this.state.activeOption) {
      optionActiveClass = styles.CardItem__options__active
    }

    return (
      <Link href="">
        <a className={styles.CardItem}>
          <div className={styles.CardItem__imageContainer}>
            {imagePreview}
          </div>
          <div className={[styles.CardItem__options, optionActiveClass].join(' ')}>
            <div className={styles.CardItem__optionsDots} key={this.props.id} onClick={e => this.optionsActive(e)}>
              <div></div>
              <div></div>
              <div></div>
            </div>
            <div className={styles.CardItem__optionsContainer}>
              <span className={styles.CardItem__optionItem}>Edit</span>
              <span className={styles.CardItem__optionItem}>Delete</span>
            </div>
          </div>
          <div className={styles.CardItem__information}>
            <h4 className={styles.CardItem__title}>{this.props.title}</h4>
            <span className={styles.CardItem__date}>Planned start date: {this.props.date}</span>
          </div>
        </a>
      </Link>
    )
  }
}

export default CardItem