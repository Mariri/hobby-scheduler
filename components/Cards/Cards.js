import Link from 'next/link'
import { Component } from 'react'

import CardItem from './CardItem/CardItem'
import Plus from '../Symbols/Plus'

import styles from '../../styles/components/Cards.module.scss'

class Cards extends Component {
  state = {
    activeCategory: false
  }

  toggleCardBlock = event => {
    this.setState({ activeCategory: !this.state.activeCategory })
  }

  render() {
    let categoryActiveClass = null
    if(this.state.activeCategory) {
      categoryActiveClass = styles.Cards__block__hide
    }

    return (
      <div className={[styles.Cards__block, categoryActiveClass].join(' ')}>
        <span className={styles.Cards__category} onClick={this.toggleCardBlock}>{this.props.category}</span>
        <div className={styles.Cards__container}>
          <CardItem title="Dummy Title Dummy Title Dummy Title" date="10/01/2020" />
          <CardItem title="Dummy Title Dummy Title Dummy Title" date="10/01/2020" />
          <CardItem title="Dummy Title Dummy Title Dummy Title" date="10/01/2020" />
          <CardItem title="Dummy Title Dummy Title Dummy Title" date="10/01/2020" />
          <CardItem title="Dummy Title Dummy Title Dummy Title" date="10/01/2020" />
          <CardItem title="Dummy Title Dummy Title Dummy Title" date="10/01/2020" />
          <CardItem title="Dummy Title Dummy Title Dummy Title" date="10/01/2020" />
          <CardItem title="Dummy Title Dummy Title Dummy Title" date="10/01/2020" />
          <CardItem title="Dummy Title Dummy Title Dummy Title" date="10/01/2020" />
          <Link href="">
            <a className={styles.Cards__add}>
              <Plus />
              <span className={styles.Cards__addText}>Add new item</span>
            </a>
          </Link>
        </div>
      </div>
    )
  }
}

export default Cards