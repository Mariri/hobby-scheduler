import { max } from 'moment'
import { Component } from 'react'

import styles from '../../styles/components/Profile.module.scss'

class StatusList extends Component {
  state = {
    active: false,
    maxHeight: 0
  }

  activeOnClick = event => {
    let content = event.currentTarget.nextElementSibling
    let contentMaxHeight = null

    if (this.state.active) {
      contentMaxHeight = null
    } else {
      contentMaxHeight = content.scrollHeight + 'px'
    }

    this.setState({
      active: !this.state.active,
      maxHeight: contentMaxHeight })
  }

  render() {
    let activeClass = null
    if(this.state.active) {
      activeClass = styles.Profile__itemActive
    }

    return (
      <div className={[styles.Profile__item, activeClass].join(' ')}>
        <div
          className={[styles.Profile__title, this.props.classname].join(' ')}
          onClick={this.activeOnClick}>{this.props.name}</div>
        <div
          className={styles.Profile__table}
          style={{
            maxHeight: this.state.maxHeight
          }}>
          <div className={styles.Profile__itemTable}>
            <span className={styles.Profile__itemTitle}>Dummy TitleDummy TitleDummy TitleDummy...</span>
            <span className={styles.Profile__category}>Anime</span>
            <span className={styles.Profile__date}>09/10/2020</span>
          </div>
          <div className={styles.Profile__itemTable}>
            <span className={styles.Profile__itemTitle}>Dummy TitleDummy TitleDummy TitleDummy...</span>
            <span className={styles.Profile__category}>Anime</span>
            <span className={styles.Profile__date}>09/10/2020</span>
          </div>
        </div>
      </div>
    )
  }
}

export default StatusList