import Header from '../components/header/header'
import Navigation from '../components/Navigation/Navigation'
import StatusList from '../components/StatusList/StatusList'

import contentStyles from '../styles/components/Content.module.scss'
import profileStyle from '../styles/components/Profile.module.scss'

const Profile = () => {
  return (
    <>
      <Header />
      <Navigation />
      <div className={contentStyles.Content}>
        <div className={profileStyle.Profile}>
          <StatusList name="Open" />
          <StatusList
            name="On Going"
            classname={profileStyle.Profile__titleYellow} />
          <StatusList
            name="Completed"
            classname={profileStyle.Profile__titleGreen} />
          <StatusList
            name="Mastered"
            classname={profileStyle.Profile__titleBlue} />
          <StatusList
            name="Abandoned"
            classname={profileStyle.Profile__titleGray} />
        </div>
      </div>
    </>
  )
}

export default Profile