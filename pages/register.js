import Link from 'next/link'
import styles from '../styles/components/Login.module.scss'
import Header from '../components/header/header'
import Logo from '../components/Logo/Logo'
import Input from '../components/Form/Input/Input'
import Button from '../components/Form/Input/Button'

const Login = () => {
  return (
    <>
      <Header />
      <form className={styles.Login}>
        <Logo />
        <div className={styles.Login__container}>
          <Input
            label="Username"
            type="text"
            name="username" />
          <Input
            label="Nickname"
            type="text"
            name="nickname" />
          <Input
            label="Password"
            type="password"
            name="password"
            isRegisterPassword="true"
            error="true" />
          <Input
            label="Confirm Password"
            type="password"
            name="confirm-password" />
        </div>
        <Button value="Submit" />
        <div className={styles.Login__text}>
          <span>You have an account?</span>
          <Link href="/login">
            <a className={styles.Login__link}>Login.</a>
          </Link>
        </div>
      </form>
    </>
  )
}

export default Login