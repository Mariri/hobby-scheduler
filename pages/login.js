import Link from 'next/link'
import styles from '../styles/components/Login.module.scss'
import Header from '../components/header/header'
import Logo from '../components/Logo/Logo'
import Input from '../components/Form/Input/Input'
import Button from '../components/Form/Input/Button'

const Login = () => {
  return (
    <>
      <Header />
      <form className={styles.Login}>
        <Logo />
        <div className={styles.Login__container}>
          <Input
            label="Username"
            type="text"
            name="username"
            error="true" />
          <Input
            label="Password"
            type="password"
            name="password" />
        </div>
        <Button value="Submit" />
        <div className={styles.Login__text}>
          <span>You don’t have an account?</span>
          <Link href="/register">
            <a className={styles.Login__link}>Register.</a>
          </Link>
        </div>
      </form>
    </>
  )
}

export default Login