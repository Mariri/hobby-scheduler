import Header from '../../components/header/header'
import Navigation from '../../components/Navigation/Navigation'
import Plus from '../../components/Symbols/Plus'
import Input from '../../components/Form/Input/Input'
import Select from '../../components/Form/Input/Select'
import Textarea from '../../components/Form/Input/Textarea'
import Datepicker from '../../components/Form/Input/Datepicker'
import Checkbox from '../../components/Form/Input/Checkbox'
import Button from '../../components/Form/Input/Button'

import contentStyle from '../../styles/components/Content.module.scss'
import hobbyStyle from '../../styles/components/Hobby.module.scss'
import { Component } from 'react'

const optionsList = [
  'Open',
  'On going',
  'Completed',
  'Mastered',
  'Abandoned'
]

const checkboxList = [
  'Anime',
  'Games',
]

class Hobby extends Component {
  state = {
    uploadedImage: '',
    errorProfileMessage: ''
  }

  getBase64 = event => {
    let file = event.target.files[0]
    let reader = new FileReader()
    if(event.target.files[0]){
      reader.readAsDataURL(file)
    }
    if(file.type.startsWith('image/')) {
      this.setState({ errorProfileMessage: '' })

      reader.onload = () => {
        this.setState({
          uploadedImage: reader.result
        })
      };

      reader.onerror = error => {
        console.log('error')
      }
    } else {
      this.setState({
        errorProfileMessage: 'Use only image file extension!'
      })
    }
  }

  render() {
    return (
      <>
        <Header />
        <Navigation />
        <div className={contentStyle.Content}>
          <div className={hobbyStyle.Hobby}>
            <div className={hobbyStyle.Hobby__settings}>
              <div className={hobbyStyle.Hobby__image}>
                {this.state.uploadedImage !== ''
                  ? <img src={this.state.uploadedImage} className={hobbyStyle.Hobby__imageUploaded} /> 
                  : ''
                }
                <input
                  type="file"
                  name="image"
                  className={hobbyStyle.Hobby__file}
                  onChange={this.getBase64}
                  accept="image/*" />
                <div className={hobbyStyle.Hobby__fileContent}>
                  <Plus />
                  <span className={hobbyStyle.Hobby__imageText}>Add image</span>
                </div>
                <span className={hobbyStyle.Hobby__error}>{this.state.errorProfileMessage}</span>
              </div>
              <div className={hobbyStyle.Hobby__input}>
                <div>
                  <Input
                    label="Title*"
                    type="text"
                    name="title" />
                  <Textarea
                    label="Description"
                    name="description" />
                </div>
                <div>
                  <Select
                    name="status"
                    label="Status"
                    options={optionsList} />
                  <Datepicker
                    label="Estimated start date"
                    name="start-date" />
                </div>
                <div className={hobbyStyle.Hobby__checkbox}>
                  <Checkbox
                    label="Category"
                    options={checkboxList} />
                </div>
              </div>
            </div>
            <div className={hobbyStyle.Hobby__button}>
              <Button value="Submit" />
            </div>
          </div>
        </div>
      </>
    )
  }
}

export default Hobby