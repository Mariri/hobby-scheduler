import Header from '../../components/header/header'
import Navigation from '../../components/Navigation/Navigation'
import Input from '../../components/Form/Input/Input'
import Button from '../../components/Form/Input/Button'
import Ripple from '../../components/Ripple/Ripple'

import contentStyle from '../../styles/components/Content.module.scss'
import personalStyle from '../../styles/components/Personal.module.scss'
import { Component } from 'react'

class Personal extends Component {
  state = {
    loading: false,
    message: '',
    uploadedImage: '',
    errorProfileMessage: ''
  }

  getBase64 = event => {
    let file = event.target.files[0]
    let reader = new FileReader()
    if(event.target.files[0]){
      reader.readAsDataURL(file)
    }
    if(file.type.startsWith('image/')) {
      this.setState({ errorProfileMessage: '' })

      reader.onload = () => {
        this.setState({
          uploadedImage: reader.result
        })
      };

      reader.onerror = error => {
        console.log('error')
      }
    } else {
      this.setState({
        errorProfileMessage: 'Use only image file extension!'
      })
    }
  }

  render() {
    let message = null
    if(this.state.loading) {
      message = <Ripple />
    } else if(this.state.message !== '') {
      message = this.state.message
    }

    return (
      <>
        <Header />
        <Navigation />
        <div className={contentStyle.Content}>
          <div className={personalStyle.Personal__header}>
            <div className={personalStyle.Personal__image}>
              {this.state.uploadedImage !== ''
                ? <img src={this.state.uploadedImage} className={personalStyle.Personal__imageUploaded} /> 
                : ''
              }
              <input
                type="file"
                name="image"
                className={personalStyle.Personal__file}
                onChange={this.getBase64}
                accept="image/*" />
              <span className={personalStyle.Personal__imageHover}>Upload image</span>
            </div>
            <h1 className={personalStyle.Personal__title}>Personal Settings</h1>
          </div>
          <span className={personalStyle.Personal__error}>{this.state.errorProfileMessage}</span>
          <form className={personalStyle.Personal__form}>
            <div className={personalStyle.Personal__inputs}>
              <Input
                name="username"
                label="Username"
                type="text" />
              <Input
                name="nickname"
                label="Nickname"
                type="text" />
            </div>
            <div className={personalStyle.Personal__buttonGroup}>
              <Button value="Change Password"  />
              <Button value="Submit" />
            </div>
            <div className={personalStyle.Personal__message}>
              {message}
            </div>
          </form>
        </div>
      </>
    )
  }
}

export default Personal