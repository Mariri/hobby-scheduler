import Header from '../components/header/header'
import Navigation from '../components/Navigation/Navigation'
import Cards from '../components/Cards/Cards'

import styles from '../styles/components/Content.module.scss'

export default function Home() {
  return (
    <>
      <Header />
      <Navigation />
      <div className={styles.Content}>
        <Cards category="Favorites" />
        <Cards category="Anime" />
      </div>
    </>
  )
}
